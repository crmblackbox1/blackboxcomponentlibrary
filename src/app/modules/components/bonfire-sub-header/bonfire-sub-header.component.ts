import { Component, Input, } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'blackbox-bonfire-sub-header',
  templateUrl: './bonfire-sub-header.component.html',
  styleUrls: ['./bonfire-sub-header.component.scss'],
})
export class BonfireSubHeaderComponent {
  @Input() addIcon;
  @Input() addTooltip: string;
  @Input() addRoute: string;
  @Input() title: string;
  @Input() hasFilter = false;

  constructor(private router: Router) { }

  route() {
    // This should probably be switched to an event emitter, and then there wouldn't be any need for the addRoute property and the Router
    this.router.navigate([this.addRoute]);
  }

}
