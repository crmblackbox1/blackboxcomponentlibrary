import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'invoiceDate'
})
export class InvoiceDatePipe implements PipeTransform {
/**
 * @param value string that looks like 2019-10-01, which is what Braintree gives
 * @param last boolen, if true, send back string with year
 *
 * Usage:
 *   value | invoiceDate
 *
 *  Example:
 *   {{'2019-09-12' | invoiceDate}}
 *   formats to: 'September 12'
 *
 * Example with last as true:
 *   {{ 2020-01-03' | invoiceDate:true }} formats to 'January 03, 2020'
 */
  transform(value: string, last: boolean = false): string {
    if (this.hasValidValue(value, last)) {
      const [year, month, date] = value.split('-');
      const longMonth = this.longMonth(month);
      return month === '12' || last ? `${longMonth} ${date}, ${year}` : `${longMonth} ${date}`;
    } else {
      throw TypeError('Invalid param types: value should be a string of numbers like 2019-10-12, and last should be a boolean');
    }
  }

  hasValidValue(value: string, last: boolean): boolean {
    return !!(typeof value === 'string' &&
              value.split('-').length === 3 &&
              typeof last === 'boolean');
  }

  longMonth(month: string): string {
    switch (month) {
      case '01': return 'January';
      case '02': return 'February';
      case '03': return 'March';
      case '04': return 'April';
      case '05': return 'May';
      case '06': return 'June';
      case '07': return 'July';
      case '08': return 'August';
      case '09': return 'September';
      case '10': return 'October';
      case '11': return 'November';
      case '12': return 'December';
      default: return '';
    }
  }

}
