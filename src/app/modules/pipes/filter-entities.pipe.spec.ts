import { FilterEntitiesPipe } from './filter-entities.pipe';
import { ColumnParams } from './interfaces/column-params.interface';

describe('FilterEntitiesPipe', () => {
  const pipe = new FilterEntitiesPipe();
  let entities: any[];
  let filter: string;
  let columnParams: ColumnParams[];

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('#transform initially without any entities returns an empty array', () => {
    entities = [];
    filter = undefined;
    columnParams = [
      {label: 'Site name', mapper: (e) => e.site_name},
      {label: 'Created at', mapper: (e) => e.createdAt},
      {label: 'Updated at', mapper: (e) => e.updatedAt},
      {label: 'Active', mapper: (e) => e.active.toString()},
    ];
    const transformed = pipe.transform(entities, filter, columnParams);
    expect(pipe.transform(entities, filter, columnParams)).toEqual([]);
  });

  it('#transform with entities but without a filter returns all the entities', () => {
    entities = [
      {
        site_name: 'test site',
        createdAt: (new Date()).toISOString(),
        updatedAt: (new Date()).toISOString(),
        active: true,
        id: 1
      }
    ];
    filter = undefined;
    const transformed = pipe.transform(entities, filter, columnParams);
    expect(transformed).toEqual(entities);
  });

  it('#isSubstring returns true if the filter string is in the value string', () => {
    expect(pipe.isSubstring('abc', 'a')).toBe(true);
  });

  it('#isSubstring returns false if the filter string is not in the value string', () => {
    expect(pipe.isSubstring('abc', 'd')).toBe(false);
  });

  it('#containsValue returns true if a property on an entity contains the filter string', () => {
    const entity = {
      site_name: 'test site a',
      createdAt: (new Date()).toISOString(),
      updatedAt: (new Date()).toISOString(),
      active: true,
      id: 1
    };
    filter = 'site a';
    expect(pipe.containsValue(entity, filter, columnParams)).toBe(true);
  });

  it('#containsValue returns false if a property on an entity does not contains the filter string', () => {
    const entity = {
      site_name: 'test site b',
      createdAt: (new Date()).toISOString(),
      updatedAt: (new Date()).toISOString(),
      active: true,
      id: 1
    };
    filter = 'site a';
    expect(pipe.containsValue(entity, filter, columnParams)).toBe(false);
  });

  it('#transform returns a subset of the entities when provided with valid filter', () => {
    entities = [
      {
        site_name: 'test site a',
        createdAt: (new Date()).toISOString(),
        updatedAt: (new Date()).toISOString(),
        active: true,
        id: 1
      },
      {
        site_name: 'test site b',
        createdAt: (new Date()).toISOString(),
        updatedAt: (new Date()).toISOString(),
        active: true,
        id: 1
      }
    ];
    filter = 'site a';
    expect(pipe.transform(entities, filter, columnParams)).toEqual([entities[0]]);
  });
});
