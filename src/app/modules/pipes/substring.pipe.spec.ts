import { SubstringPipe } from './substring.pipe';

describe('SubstringPipe', () => {
  const pipe = new SubstringPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return the string if the start character has no instances in the value param', () => {
    expect(pipe.transform('a')).toBe('a');
  });

  it('should return the last part of the path of a url', () => {
    const url = 'https://s3.amazonaws.com/ibogenerator/resources/template/image/logos/placeholderlogo.png';
    const result = 'placeholderlogo.png';
    expect(pipe.transform(url)).toBe(result);
  });

  it('should return with a different value if the startCharacter is defined', () => {
    const value = 'abcd';
    expect(pipe.transform(value, 'b')).toBe('cd');
  });
});
