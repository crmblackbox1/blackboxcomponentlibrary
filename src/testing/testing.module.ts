import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLinkStubDirective, RouterOutletStubComponent } from './router-stubs';

@NgModule({
  declarations: [
    RouterLinkStubDirective,
    RouterOutletStubComponent,
  ]
})
export class TestingModule {}
