import { Component, NgModule, Pipe } from '@angular/core';
import { CommonModule } from '@angular/common';
var FooComponent = (function () {
    function FooComponent() {
    }
    /**
     * @return {?}
     */
    FooComponent.prototype.ngOnInit = function () {
    };
    return FooComponent;
}());
FooComponent.decorators = [
    { type: Component, args: [{
                selector: 'blackbox-foo',
                template: "\n    <p>\n      foo works!\n    </p>\n  ",
                styles: ["\n\n  "]
            },] },
];
/**
 * @nocollapse
 */
FooComponent.ctorParameters = function () { return []; };
var FooModule = (function () {
    function FooModule() {
    }
    return FooModule;
}());
FooModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [FooComponent],
                exports: [FooComponent]
            },] },
];
/**
 * @nocollapse
 */
FooModule.ctorParameters = function () { return []; };
/**
 * Return the last path of the url
 * @param value url
 * @param startCharacter: a character that will be used to split on
 * 	if undefined, default with '/' character
 *
 * Usage:
 *   value | substring
 *
 *  Example:
 *   {{'https://s3.amazonaws.com/placeholderlogo.png' | substring:'/' }}
 *   formats to: 'placeholderlogo.png'
 *
 * Example with different startCharacter:
 *   {{ 'abcd' | substring: 'b' }} formats to 'cd'
 */
var SubstringPipe = (function () {
    function SubstringPipe() {
    }
    /**
     * @param {?} value
     * @param {?=} startCharacter
     * @return {?}
     */
    SubstringPipe.prototype.transform = function (value, startCharacter) {
        if (startCharacter === void 0) { startCharacter = '/'; }
        return value.substring(value.lastIndexOf(startCharacter) + 1);
    };
    return SubstringPipe;
}());
SubstringPipe.decorators = [
    { type: Pipe, args: [{
                name: 'substring'
            },] },
];
/**
 * @nocollapse
 */
SubstringPipe.ctorParameters = function () { return []; };
var InvoiceDatePipe = (function () {
    function InvoiceDatePipe() {
    }
    /**
     * @param {?} value string that looks like 2019-10-01, which is what Braintree gives
     * @param {?=} last boolen, if true, send back string with year
     *
     * Usage:
     *   value | invoiceDate
     *
     *  Example:
     *   {{'2019-09-12' | invoiceDate}}
     *   formats to: 'September 12'
     *
     * Example with last as true:
     *   {{ 2020-01-03' | invoiceDate:true }} formats to 'January 03, 2020'
     * @return {?}
     */
    InvoiceDatePipe.prototype.transform = function (value, last) {
        if (last === void 0) { last = false; }
        if (this.hasValidValue(value, last)) {
            var _a = value.split('-'), year = _a[0], month = _a[1], date = _a[2];
            var /** @type {?} */ longMonth = this.longMonth(month);
            return month === '12' || last ? longMonth + " " + date + ", " + year : longMonth + " " + date;
        }
        else {
            throw TypeError('Invalid param types: value should be a string of numbers like 2019-10-12, and last should be a boolean');
        }
    };
    /**
     * @param {?} value
     * @param {?} last
     * @return {?}
     */
    InvoiceDatePipe.prototype.hasValidValue = function (value, last) {
        return !!(typeof value === 'string' &&
            value.split('-').length === 3 &&
            typeof last === 'boolean');
    };
    /**
     * @param {?} month
     * @return {?}
     */
    InvoiceDatePipe.prototype.longMonth = function (month) {
        switch (month) {
            case '01': return 'January';
            case '02': return 'February';
            case '03': return 'March';
            case '04': return 'April';
            case '05': return 'May';
            case '06': return 'June';
            case '07': return 'July';
            case '08': return 'August';
            case '09': return 'September';
            case '10': return 'October';
            case '11': return 'November';
            case '12': return 'December';
            default: return '';
        }
    };
    return InvoiceDatePipe;
}());
InvoiceDatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'invoiceDate'
            },] },
];
/**
 * @nocollapse
 */
InvoiceDatePipe.ctorParameters = function () { return []; };
var SortEntitiesPipe = (function () {
    function SortEntitiesPipe() {
    }
    /**
     * @param {?=} entities
     * @param {?=} sortBy
     * @param {?=} order
     * @return {?}
     */
    SortEntitiesPipe.prototype.transform = function (entities, sortBy, order) {
        if (entities === void 0) { entities = []; }
        if (!sortBy) {
            return entities;
        }
        if (entities.length === 0) {
            return [];
        }
        var /** @type {?} */ comparator = sortBy.comparator || this.alphabeticComparator(sortBy.mapper);
        return entities.sort(function (e1, e2) {
            var /** @type {?} */ result = comparator(e1, e2);
            if (result === 0) {
                return 0;
            }
            if (result === 1) {
                return order === 'asc' ? 1 : -1;
            }
            if (result === -1) {
                return order === 'asc' ? -1 : 1;
            }
        });
    };
    /**
     * @param {?} mapper
     * @return {?}
     */
    SortEntitiesPipe.prototype.alphabeticComparator = function (mapper) {
        return function (entity1, entity2) {
            // get the value of the row,
            // set all letters to uppercase so the comparison doesn't care about case
            // mapper looks like (entity) => entity.property
            var /** @type {?} */ first = mapper(entity1).toUpperCase();
            var /** @type {?} */ second = mapper(entity2).toUpperCase();
            if (first < second) {
                return -1;
            }
            if (first > second) {
                return 1;
            }
            return 0;
        };
    };
    return SortEntitiesPipe;
}());
SortEntitiesPipe.decorators = [
    { type: Pipe, args: [{
                name: 'sortEntities'
            },] },
];
/**
 * @nocollapse
 */
SortEntitiesPipe.ctorParameters = function () { return []; };
var FilterEntitiesPipe = (function () {
    function FilterEntitiesPipe() {
    }
    /**
     * @param {?} entity
     * @param {?} filter
     * @param {?} columnParams
     * @return {?}
     */
    FilterEntitiesPipe.prototype.containsValue = function (entity, filter, columnParams) {
        var _this = this;
        var /** @type {?} */ contains = false;
        columnParams.forEach(function (param) {
            if (_this.isSubstring(param.mapper(entity), filter)) {
                contains = true;
                return;
            }
        });
        return contains;
    };
    /**
     * @param {?} value
     * @param {?} filter
     * @return {?}
     */
    FilterEntitiesPipe.prototype.isSubstring = function (value, filter) {
        if (typeof value === 'string' && typeof filter === 'string') {
            var /** @type {?} */ propertyValue = value.toLowerCase();
            var /** @type {?} */ stringFilter = filter.toLowerCase();
            return propertyValue.includes(stringFilter);
        }
        else {
            throw Error("Invalid types, value: " + typeof value);
        }
    };
    /**
     * @param {?=} entities
     * @param {?=} filter
     * @param {?=} columnParams
     * @return {?}
     */
    FilterEntitiesPipe.prototype.transform = function (entities, filter, columnParams) {
        var _this = this;
        if (entities === void 0) { entities = []; }
        if (!filter || !columnParams) {
            return entities;
        }
        else if (entities.length === 0) {
            return [];
        }
        else {
            return entities.filter(function (entity) { return _this.containsValue(entity, filter, columnParams); });
        }
    };
    return FilterEntitiesPipe;
}());
FilterEntitiesPipe.decorators = [
    { type: Pipe, args: [{
                name: 'filterEntities'
            },] },
];
/**
 * @nocollapse
 */
FilterEntitiesPipe.ctorParameters = function () { return []; };
// import { SafeurlPipe } from './safeurl.pipe';
var PipesModule = (function () {
    function PipesModule() {
    }
    return PipesModule;
}());
PipesModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [
                    SubstringPipe,
                    InvoiceDatePipe,
                    SortEntitiesPipe,
                    FilterEntitiesPipe,
                ],
                exports: [
                    SubstringPipe,
                    InvoiceDatePipe,
                    SortEntitiesPipe,
                    FilterEntitiesPipe,
                ]
            },] },
];
/**
 * @nocollapse
 */
PipesModule.ctorParameters = function () { return []; };
/**
 * Generated bundle index. Do not edit.
 */
export { FooModule, PipesModule, FooComponent as ɵa, FilterEntitiesPipe as ɵe, InvoiceDatePipe as ɵc, SortEntitiesPipe as ɵd, SubstringPipe as ɵb };
//# sourceMappingURL=blackbox-component-library.es5.js.map
