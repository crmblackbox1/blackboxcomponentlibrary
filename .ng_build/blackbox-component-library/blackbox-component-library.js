import { Component, NgModule, Pipe } from '@angular/core';
import { CommonModule } from '@angular/common';

class FooComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
FooComponent.decorators = [
    { type: Component, args: [{
                selector: 'blackbox-foo',
                template: `
    <p>
      foo works!
    </p>
  `,
                styles: [`

  `]
            },] },
];
/**
 * @nocollapse
 */
FooComponent.ctorParameters = () => [];

class FooModule {
}
FooModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [FooComponent],
                exports: [FooComponent]
            },] },
];
/**
 * @nocollapse
 */
FooModule.ctorParameters = () => [];

/**
 * Return the last path of the url
 * @param value url
 * @param startCharacter: a character that will be used to split on
 * 	if undefined, default with '/' character
 *
 * Usage:
 *   value | substring
 *
 *  Example:
 *   {{'https://s3.amazonaws.com/placeholderlogo.png' | substring:'/' }}
 *   formats to: 'placeholderlogo.png'
 *
 * Example with different startCharacter:
 *   {{ 'abcd' | substring: 'b' }} formats to 'cd'
 */
class SubstringPipe {
    /**
     * @param {?} value
     * @param {?=} startCharacter
     * @return {?}
     */
    transform(value, startCharacter = '/') {
        return value.substring(value.lastIndexOf(startCharacter) + 1);
    }
}
SubstringPipe.decorators = [
    { type: Pipe, args: [{
                name: 'substring'
            },] },
];
/**
 * @nocollapse
 */
SubstringPipe.ctorParameters = () => [];

class InvoiceDatePipe {
    /**
     * @param {?} value string that looks like 2019-10-01, which is what Braintree gives
     * @param {?=} last boolen, if true, send back string with year
     *
     * Usage:
     *   value | invoiceDate
     *
     *  Example:
     *   {{'2019-09-12' | invoiceDate}}
     *   formats to: 'September 12'
     *
     * Example with last as true:
     *   {{ 2020-01-03' | invoiceDate:true }} formats to 'January 03, 2020'
     * @return {?}
     */
    transform(value, last = false) {
        if (this.hasValidValue(value, last)) {
            const [year, month, date] = value.split('-');
            const /** @type {?} */ longMonth = this.longMonth(month);
            return month === '12' || last ? `${longMonth} ${date}, ${year}` : `${longMonth} ${date}`;
        }
        else {
            throw TypeError('Invalid param types: value should be a string of numbers like 2019-10-12, and last should be a boolean');
        }
    }
    /**
     * @param {?} value
     * @param {?} last
     * @return {?}
     */
    hasValidValue(value, last) {
        return !!(typeof value === 'string' &&
            value.split('-').length === 3 &&
            typeof last === 'boolean');
    }
    /**
     * @param {?} month
     * @return {?}
     */
    longMonth(month) {
        switch (month) {
            case '01': return 'January';
            case '02': return 'February';
            case '03': return 'March';
            case '04': return 'April';
            case '05': return 'May';
            case '06': return 'June';
            case '07': return 'July';
            case '08': return 'August';
            case '09': return 'September';
            case '10': return 'October';
            case '11': return 'November';
            case '12': return 'December';
            default: return '';
        }
    }
}
InvoiceDatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'invoiceDate'
            },] },
];
/**
 * @nocollapse
 */
InvoiceDatePipe.ctorParameters = () => [];

class SortEntitiesPipe {
    /**
     * @param {?=} entities
     * @param {?=} sortBy
     * @param {?=} order
     * @return {?}
     */
    transform(entities = [], sortBy, order) {
        if (!sortBy) {
            return entities;
        }
        if (entities.length === 0) {
            return [];
        }
        const /** @type {?} */ comparator = sortBy.comparator || this.alphabeticComparator(sortBy.mapper);
        return entities.sort((e1, e2) => {
            const /** @type {?} */ result = comparator(e1, e2);
            if (result === 0) {
                return 0;
            }
            if (result === 1) {
                return order === 'asc' ? 1 : -1;
            }
            if (result === -1) {
                return order === 'asc' ? -1 : 1;
            }
        });
    }
    /**
     * @param {?} mapper
     * @return {?}
     */
    alphabeticComparator(mapper) {
        return (entity1, entity2) => {
            // get the value of the row,
            // set all letters to uppercase so the comparison doesn't care about case
            // mapper looks like (entity) => entity.property
            const /** @type {?} */ first = mapper(entity1).toUpperCase();
            const /** @type {?} */ second = mapper(entity2).toUpperCase();
            if (first < second) {
                return -1;
            }
            if (first > second) {
                return 1;
            }
            return 0;
        };
    }
}
SortEntitiesPipe.decorators = [
    { type: Pipe, args: [{
                name: 'sortEntities'
            },] },
];
/**
 * @nocollapse
 */
SortEntitiesPipe.ctorParameters = () => [];

class FilterEntitiesPipe {
    /**
     * @param {?} entity
     * @param {?} filter
     * @param {?} columnParams
     * @return {?}
     */
    containsValue(entity, filter, columnParams) {
        let /** @type {?} */ contains = false;
        columnParams.forEach(param => {
            if (this.isSubstring(param.mapper(entity), filter)) {
                contains = true;
                return;
            }
        });
        return contains;
    }
    /**
     * @param {?} value
     * @param {?} filter
     * @return {?}
     */
    isSubstring(value, filter) {
        if (typeof value === 'string' && typeof filter === 'string') {
            const /** @type {?} */ propertyValue = value.toLowerCase();
            const /** @type {?} */ stringFilter = filter.toLowerCase();
            return propertyValue.includes(stringFilter);
        }
        else {
            throw Error(`Invalid types, value: ${typeof value}`);
        }
    }
    /**
     * @param {?=} entities
     * @param {?=} filter
     * @param {?=} columnParams
     * @return {?}
     */
    transform(entities = [], filter, columnParams) {
        if (!filter || !columnParams) {
            return entities;
        }
        else if (entities.length === 0) {
            return [];
        }
        else {
            return entities.filter(entity => this.containsValue(entity, filter, columnParams));
        }
    }
}
FilterEntitiesPipe.decorators = [
    { type: Pipe, args: [{
                name: 'filterEntities'
            },] },
];
/**
 * @nocollapse
 */
FilterEntitiesPipe.ctorParameters = () => [];

// import { SafeurlPipe } from './safeurl.pipe';
class PipesModule {
}
PipesModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [
                    SubstringPipe,
                    InvoiceDatePipe,
                    SortEntitiesPipe,
                    FilterEntitiesPipe,
                ],
                exports: [
                    SubstringPipe,
                    InvoiceDatePipe,
                    SortEntitiesPipe,
                    FilterEntitiesPipe,
                ]
            },] },
];
/**
 * @nocollapse
 */
PipesModule.ctorParameters = () => [];

/**
 * Generated bundle index. Do not edit.
 */

export { FooModule, PipesModule, FooComponent as ɵa, FilterEntitiesPipe as ɵe, InvoiceDatePipe as ɵc, SortEntitiesPipe as ɵd, SubstringPipe as ɵb };
//# sourceMappingURL=blackbox-component-library.js.map
