/**
 * Generated bundle index. Do not edit.
 */
export { FooModule, PipesModule } from './public_api';
export { FooComponent as ɵa } from './src/app/modules/foo/foo.component';
export { FilterEntitiesPipe as ɵe } from './src/app/modules/pipes/filter-entities.pipe';
export { InvoiceDatePipe as ɵc } from './src/app/modules/pipes/invoice-date.pipe';
export { SortEntitiesPipe as ɵd } from './src/app/modules/pipes/sort-entities.pipe';
export { SubstringPipe as ɵb } from './src/app/modules/pipes/substring.pipe';
//# sourceMappingURL=blackbox-component-library.js.map
