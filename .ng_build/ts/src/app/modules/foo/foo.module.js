import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooComponent } from './foo.component';
export class FooModule {
}
FooModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [FooComponent],
                exports: [FooComponent]
            },] },
];
/**
 * @nocollapse
 */
FooModule.ctorParameters = () => [];
function FooModule_tsickle_Closure_declarations() {
    /** @type {?} */
    FooModule.decorators;
    /**
     * @nocollapse
     * @type {?}
     */
    FooModule.ctorParameters;
}
//# sourceMappingURL=foo.module.js.map