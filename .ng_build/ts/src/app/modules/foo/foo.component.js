import { Component } from '@angular/core';
export class FooComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
FooComponent.decorators = [
    { type: Component, args: [{
                selector: 'blackbox-foo',
                template: `
    <p>
      foo works!
    </p>
  `,
                styles: [`

  `]
            },] },
];
/**
 * @nocollapse
 */
FooComponent.ctorParameters = () => [];
function FooComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    FooComponent.decorators;
    /**
     * @nocollapse
     * @type {?}
     */
    FooComponent.ctorParameters;
}
//# sourceMappingURL=foo.component.js.map