import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BonfireSubHeaderComponent } from './bonfire-sub-header/bonfire-sub-header.component';
import {
  MatIconModule,
  MatButtonModule,
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
  ],
  declarations: [BonfireSubHeaderComponent],
  exports: [
    BonfireSubHeaderComponent
  ]
})
export class ComponentsModule { }
