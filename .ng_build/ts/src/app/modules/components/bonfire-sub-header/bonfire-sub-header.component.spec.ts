import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { Router } from '@angular/router';
import { BonfireSubHeaderComponent } from './bonfire-sub-header.component';
import { MatButtonModule} from '@angular/material';
import { RouterStub } from '../../../../testing/router-stubs';

describe('BonfireSubHeaderComponent', () => {
  let component: BonfireSubHeaderComponent;
  let fixture: ComponentFixture<BonfireSubHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonfireSubHeaderComponent ],
      imports: [ MatButtonModule ],
      providers: [
        { provide: Router, useClass: RouterStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonfireSubHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#route sends the user to the form to add a new entity', inject([Router], (router: Router) => {
    component.addRoute = 'entities/add';
    fixture.detectChanges();
    const spy = spyOn(router, 'navigate');
    component.route();
    expect(spy.calls.any()).toBe(true);
    expect(spy.calls.first().args[0]).toEqual(['entities/add']);
  }));
});
