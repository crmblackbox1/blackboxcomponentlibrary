import { Component, Input, } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'blackbox-bonfire-sub-header',
  template: `
    <div class="subheader">
    	<div class="subheader__action">
    		<button (click)="route()"
    			md-mini-fab
    			class="subheader__button">
    			<md-icon>{{addIcon}}</md-icon>
    		</button>
    	</div>
    	<div>
    		<h2 class="subheader__title">{{title}}</h2>
    	</div>
    </div>
  `,
  styles: [`
    .subheader {
      background-color: #252B33;
      height: 40px;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: horizontal;
      -webkit-box-direction: normal;
          -ms-flex-direction: row;
              flex-direction: row;
      -ms-flex-wrap: nowrap;
          flex-wrap: nowrap;
      -webkit-box-pack: start;
          -ms-flex-pack: start;
              justify-content: flex-start;
      -ms-flex-line-pack: start;
          align-content: flex-start;
      -webkit-box-align: center;
          -ms-flex-align: center;
              align-items: center; }
      .subheader__title {
        padding-left: 10px;
        color: #AAABAE;
        font-family: 'Roboto';
        font-size: 24px; }
      .subheader__title:hover {
        color: #FFFFFF; }
      .subheader__action {
        width: 70px; }
      .subheader__button {
        position: relative;
        top: 20px;
        z-index: 1;
        margin: 0 16px;
        background-color: #29ABE2;
        color: #FFFFFF; }
  `],
})
export class BonfireSubHeaderComponent {
  @Input() addIcon;
  @Input() addTooltip: string;
  @Input() addRoute: string;
  @Input() title: string;
  @Input() hasFilter = false;

  constructor(private router: Router) { }

  route() {
    // This should probably be switched to an event emitter, and then there wouldn't be any need for the addRoute property and the Router
    this.router.navigate([this.addRoute]);
  }

}
