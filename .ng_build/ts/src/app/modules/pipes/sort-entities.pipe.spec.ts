import { SortEntitiesPipe } from './sort-entities.pipe';

describe('SortEntitiesPipe', () => {
  const pipe = new SortEntitiesPipe();
  let entities;
  let sortBy;
  let order;
  const columnParams = [
    {label: 'Site name', mapper: (e) => e.site_name},
    {label: 'Created at', mapper: (e) => e.createdAt},
    {label: 'Updated at', mapper: (e) => e.updatedAt},
    {label: 'Active', mapper: (e) => e.active.toString()},
  ];

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('#transform with undefined sortBy returns all entities', () => {
    entities = [
      {
        site_name: 'test site a',
        url: 'testsitea.com',
        createdAt: (new Date()).toISOString(),
        updatedAt: (new Date()).toISOString(),
        active: true,
        id: 1
      },
      {
        site_name: 'test site b',
        url: 'testsiteb.com',
        createdAt: (new Date()).toISOString(),
        updatedAt: (new Date()).toISOString(),
        active: true,
        id: 1
      }
    ];
    order = 'asc';
    expect(pipe.transform(entities, sortBy, order)).toEqual(entities);
  });

  it('#transform with zero entities returns an empty array', () => {
    entities = [];
    order = 'asc';
    expect(pipe.transform(entities, sortBy, order)).toEqual([]);
  });

  it('#alphabeticComparator sorts entities alphabetically', () => {
    const e1 = {
      name: 'abc',
    };
    const e2 = {
      name: 'def',
    };
    const e3 = {
      name: 'abc',
    };

    function mapper(entity) {
      return entity.name;
    }

    expect(pipe.alphabeticComparator(mapper)(e1, e2)).toBe(-1);
    expect(pipe.alphabeticComparator(mapper)(e2, e1)).toBe(1);
    expect(pipe.alphabeticComparator(mapper)(e1, e3)).toBe(0);
  });

});
