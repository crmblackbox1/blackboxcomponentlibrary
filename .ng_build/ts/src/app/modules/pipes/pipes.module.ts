import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubstringPipe } from './substring.pipe';
import { InvoiceDatePipe } from './invoice-date.pipe';
// import { SafeurlPipe } from './safeurl.pipe';
import { SortEntitiesPipe } from './sort-entities.pipe';
import { FilterEntitiesPipe } from './filter-entities.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SubstringPipe,
    InvoiceDatePipe,
    SortEntitiesPipe,
    FilterEntitiesPipe,
  ],
  exports: [
    SubstringPipe,
    InvoiceDatePipe,
    SortEntitiesPipe,
    FilterEntitiesPipe,
  ]
})
export class PipesModule { }
