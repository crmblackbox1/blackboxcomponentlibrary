import { Pipe } from '@angular/core';
export class FilterEntitiesPipe {
    /**
     * @param {?} entity
     * @param {?} filter
     * @param {?} columnParams
     * @return {?}
     */
    containsValue(entity, filter, columnParams) {
        let /** @type {?} */ contains = false;
        columnParams.forEach(param => {
            if (this.isSubstring(param.mapper(entity), filter)) {
                contains = true;
                return;
            }
        });
        return contains;
    }
    /**
     * @param {?} value
     * @param {?} filter
     * @return {?}
     */
    isSubstring(value, filter) {
        if (typeof value === 'string' && typeof filter === 'string') {
            const /** @type {?} */ propertyValue = value.toLowerCase();
            const /** @type {?} */ stringFilter = filter.toLowerCase();
            return propertyValue.includes(stringFilter);
        }
        else {
            throw Error(`Invalid types, value: ${typeof value}`);
        }
    }
    /**
     * @param {?=} entities
     * @param {?=} filter
     * @param {?=} columnParams
     * @return {?}
     */
    transform(entities = [], filter, columnParams) {
        if (!filter || !columnParams) {
            return entities;
        }
        else if (entities.length === 0) {
            return [];
        }
        else {
            return entities.filter(entity => this.containsValue(entity, filter, columnParams));
        }
    }
}
FilterEntitiesPipe.decorators = [
    { type: Pipe, args: [{
                name: 'filterEntities'
            },] },
];
/**
 * @nocollapse
 */
FilterEntitiesPipe.ctorParameters = () => [];
function FilterEntitiesPipe_tsickle_Closure_declarations() {
    /** @type {?} */
    FilterEntitiesPipe.decorators;
    /**
     * @nocollapse
     * @type {?}
     */
    FilterEntitiesPipe.ctorParameters;
}
//# sourceMappingURL=filter-entities.pipe.js.map