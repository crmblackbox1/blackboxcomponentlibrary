import { InvoiceDatePipe } from './invoice-date.pipe';

describe('InvoiceDatePipe', () => {
  const pipe = new InvoiceDatePipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('#transform takes a the string 2019-09-12 and returns September 12', () => {
    const value = '2019-09-12';
    expect(pipe.transform(value)).toBe('September 12');
  });

  it('#transform takes a value like "2019-12-31" and returns "December 31, 2019"', () => {
    expect(pipe.transform('2019-12-31')).toBe('December 31, 2019');
  });

  it('#transform with last as true takes a value like "2020-01-03" and returns "January 03, 2020"', () => {
    expect(pipe.transform('2020-01-03', true)).toBe('January 03, 2020');
  });

  it('#longMonth 02', () => {
    expect(pipe.longMonth('02')).toBe('February');
  });

  it('#longMonth 03', () => {
    expect(pipe.longMonth('03')).toBe('March');
  });

  it('#longMonth 04', () => {
    expect(pipe.longMonth('04')).toBe('April');
  });

  it('#longMonth 05', () => {
    expect(pipe.longMonth('05')).toBe('May');
  });

  it('#longMonth 06', () => {
    expect(pipe.longMonth('06')).toBe('June');
  });

  it('#longMonth 07', () => {
    expect(pipe.longMonth('07')).toBe('July');
  });

  it('#longMonth 08', () => {
    expect(pipe.longMonth('08')).toBe('August');
  });

  it('#longMonth 12', () => {
    expect(pipe.longMonth('10')).toBe('October');
  });

  it('#longMonth 11', () => {
    expect(pipe.longMonth('11')).toBe('November');
  });

  it('#longMonth empty string', () => {
    expect(pipe.longMonth('')).toBe('');
  });

  it('#hasValidValue returns true with valid values', () => {
    expect(pipe.hasValidValue('2019-10-01', true)).toBe(true);
  });

  it('#hasValidValue returns false with invalid value', () => {
    expect(pipe.hasValidValue(new Date().toString(), true)).toBe(false);
  });
});
