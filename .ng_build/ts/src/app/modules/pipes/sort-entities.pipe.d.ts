import { PipeTransform } from '@angular/core';
import { ColumnParams } from './interfaces/column-params.interface';
export declare class SortEntitiesPipe implements PipeTransform {
    transform(entities: any[], sortBy: ColumnParams, order: string): any;
    alphabeticComparator(mapper: (e) => string): (e1, e2) => number;
}
