import { Pipe, PipeTransform } from '@angular/core';
import { ColumnParams } from './interfaces/column-params.interface';

@Pipe({
  name: 'filterEntities'
})
export class FilterEntitiesPipe implements PipeTransform {

  containsValue(entity: any, filter: string, columnParams: ColumnParams[]): boolean {
    let contains = false;

    columnParams.forEach(param => {
      if (this.isSubstring(param.mapper(entity), filter)) {
        contains = true;
        return;
      }
    });

    return contains;
  }

  isSubstring(value: string, filter: string): boolean {
    if (typeof value === 'string' && typeof filter === 'string') {
      const propertyValue = value.toLowerCase();
      const stringFilter = filter.toLowerCase();

      return propertyValue.includes(stringFilter);
    } else {
      throw Error(`Invalid types, value: ${typeof value}`);
    }

  }

  transform(entities: any[] = [], filter: string, columnParams: ColumnParams[]): any[] {
    if (!filter || !columnParams) {
      return entities;
    } else if (entities.length === 0) {
      return [];
    } else {
      return entities.filter(entity => this.containsValue(entity, filter, columnParams));
    }
  }

}
