import { Pipe, PipeTransform } from '@angular/core';
import { ColumnParams } from './interfaces/column-params.interface';

@Pipe({
  name: 'sortEntities'
})
export class SortEntitiesPipe implements PipeTransform {

  transform(entities: any[] = [], sortBy: ColumnParams, order: string): any {
    if (!sortBy) {
      return entities;
    }

    if (entities.length === 0) {
      return [];
    }

    const comparator = sortBy.comparator || this.alphabeticComparator(sortBy.mapper);

    return entities.sort((e1, e2) => {
      const result = comparator(e1, e2);

      if (result === 0) {
        return 0;
      }
      if (result === 1) {
        return order === 'asc' ? 1 : -1;
      }
      if (result === -1) {
        return order === 'asc' ? -1 : 1;
      }
    });
  }

  alphabeticComparator(mapper: (e) => string): (e1, e2) => number {

    return (entity1: any, entity2: any) => {
      // get the value of the row,
      // set all letters to uppercase so the comparison doesn't care about case
      // mapper looks like (entity) => entity.property
      const first = mapper(entity1).toUpperCase();
      const second = mapper(entity2).toUpperCase();

      if (first < second) {
        return -1;
      }

      if (first > second) {
        return 1;
      }

      return 0;

    };

  }

}
