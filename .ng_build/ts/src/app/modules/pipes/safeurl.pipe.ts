// import { Pipe, PipeTransform } from '@angular/core';
// import { DomSanitizer } from '@angular/platform-browser';

// @Pipe({
//   name: 'safeurl'
// })
// export class SafeurlPipe implements PipeTransform {

//   constructor(private sanitizer: DomSanitizer) {}

//   /**
//    * @param url string, return a safe version so it can be used in an iframe element
//    */
//   transform(url: string) {
//     return this.sanitizer.bypassSecurityTrustResourceUrl(url);
//   }

// }
