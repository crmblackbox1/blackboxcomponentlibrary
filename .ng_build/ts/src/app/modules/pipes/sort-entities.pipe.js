import { Pipe } from '@angular/core';
export class SortEntitiesPipe {
    /**
     * @param {?=} entities
     * @param {?=} sortBy
     * @param {?=} order
     * @return {?}
     */
    transform(entities = [], sortBy, order) {
        if (!sortBy) {
            return entities;
        }
        if (entities.length === 0) {
            return [];
        }
        const /** @type {?} */ comparator = sortBy.comparator || this.alphabeticComparator(sortBy.mapper);
        return entities.sort((e1, e2) => {
            const /** @type {?} */ result = comparator(e1, e2);
            if (result === 0) {
                return 0;
            }
            if (result === 1) {
                return order === 'asc' ? 1 : -1;
            }
            if (result === -1) {
                return order === 'asc' ? -1 : 1;
            }
        });
    }
    /**
     * @param {?} mapper
     * @return {?}
     */
    alphabeticComparator(mapper) {
        return (entity1, entity2) => {
            // get the value of the row,
            // set all letters to uppercase so the comparison doesn't care about case
            // mapper looks like (entity) => entity.property
            const /** @type {?} */ first = mapper(entity1).toUpperCase();
            const /** @type {?} */ second = mapper(entity2).toUpperCase();
            if (first < second) {
                return -1;
            }
            if (first > second) {
                return 1;
            }
            return 0;
        };
    }
}
SortEntitiesPipe.decorators = [
    { type: Pipe, args: [{
                name: 'sortEntities'
            },] },
];
/**
 * @nocollapse
 */
SortEntitiesPipe.ctorParameters = () => [];
function SortEntitiesPipe_tsickle_Closure_declarations() {
    /** @type {?} */
    SortEntitiesPipe.decorators;
    /**
     * @nocollapse
     * @type {?}
     */
    SortEntitiesPipe.ctorParameters;
}
//# sourceMappingURL=sort-entities.pipe.js.map