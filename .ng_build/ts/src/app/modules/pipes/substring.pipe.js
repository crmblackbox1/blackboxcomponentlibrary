import { Pipe } from '@angular/core';
/**
 * Return the last path of the url
 * @param value url
 * @param startCharacter: a character that will be used to split on
 * 	if undefined, default with '/' character
 *
 * Usage:
 *   value | substring
 *
 *  Example:
 *   {{'https://s3.amazonaws.com/placeholderlogo.png' | substring:'/' }}
 *   formats to: 'placeholderlogo.png'
 *
 * Example with different startCharacter:
 *   {{ 'abcd' | substring: 'b' }} formats to 'cd'
 */
export class SubstringPipe {
    /**
     * @param {?} value
     * @param {?=} startCharacter
     * @return {?}
     */
    transform(value, startCharacter = '/') {
        return value.substring(value.lastIndexOf(startCharacter) + 1);
    }
}
SubstringPipe.decorators = [
    { type: Pipe, args: [{
                name: 'substring'
            },] },
];
/**
 * @nocollapse
 */
SubstringPipe.ctorParameters = () => [];
function SubstringPipe_tsickle_Closure_declarations() {
    /** @type {?} */
    SubstringPipe.decorators;
    /**
     * @nocollapse
     * @type {?}
     */
    SubstringPipe.ctorParameters;
}
//# sourceMappingURL=substring.pipe.js.map