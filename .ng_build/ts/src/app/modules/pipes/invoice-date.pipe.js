import { Pipe } from '@angular/core';
export class InvoiceDatePipe {
    /**
     * @param {?} value string that looks like 2019-10-01, which is what Braintree gives
     * @param {?=} last boolen, if true, send back string with year
     *
     * Usage:
     *   value | invoiceDate
     *
     *  Example:
     *   {{'2019-09-12' | invoiceDate}}
     *   formats to: 'September 12'
     *
     * Example with last as true:
     *   {{ 2020-01-03' | invoiceDate:true }} formats to 'January 03, 2020'
     * @return {?}
     */
    transform(value, last = false) {
        if (this.hasValidValue(value, last)) {
            const [year, month, date] = value.split('-');
            const /** @type {?} */ longMonth = this.longMonth(month);
            return month === '12' || last ? `${longMonth} ${date}, ${year}` : `${longMonth} ${date}`;
        }
        else {
            throw TypeError('Invalid param types: value should be a string of numbers like 2019-10-12, and last should be a boolean');
        }
    }
    /**
     * @param {?} value
     * @param {?} last
     * @return {?}
     */
    hasValidValue(value, last) {
        return !!(typeof value === 'string' &&
            value.split('-').length === 3 &&
            typeof last === 'boolean');
    }
    /**
     * @param {?} month
     * @return {?}
     */
    longMonth(month) {
        switch (month) {
            case '01': return 'January';
            case '02': return 'February';
            case '03': return 'March';
            case '04': return 'April';
            case '05': return 'May';
            case '06': return 'June';
            case '07': return 'July';
            case '08': return 'August';
            case '09': return 'September';
            case '10': return 'October';
            case '11': return 'November';
            case '12': return 'December';
            default: return '';
        }
    }
}
InvoiceDatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'invoiceDate'
            },] },
];
/**
 * @nocollapse
 */
InvoiceDatePipe.ctorParameters = () => [];
function InvoiceDatePipe_tsickle_Closure_declarations() {
    /** @type {?} */
    InvoiceDatePipe.decorators;
    /**
     * @nocollapse
     * @type {?}
     */
    InvoiceDatePipe.ctorParameters;
}
//# sourceMappingURL=invoice-date.pipe.js.map