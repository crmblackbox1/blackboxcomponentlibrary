export interface ColumnParams {
  // header of a column
  label: string;

  // function that fetches field values of a column
  mapper: (entity: any) => string;

  // compare function needed for sorting, if non is provided, tables will be sorted alphabetically
  comparator?: (entity1: any, entity2: any) => number;
}
