import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubstringPipe } from './substring.pipe';
import { InvoiceDatePipe } from './invoice-date.pipe';
// import { SafeurlPipe } from './safeurl.pipe';
import { SortEntitiesPipe } from './sort-entities.pipe';
import { FilterEntitiesPipe } from './filter-entities.pipe';
export class PipesModule {
}
PipesModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [
                    SubstringPipe,
                    InvoiceDatePipe,
                    SortEntitiesPipe,
                    FilterEntitiesPipe,
                ],
                exports: [
                    SubstringPipe,
                    InvoiceDatePipe,
                    SortEntitiesPipe,
                    FilterEntitiesPipe,
                ]
            },] },
];
/**
 * @nocollapse
 */
PipesModule.ctorParameters = () => [];
function PipesModule_tsickle_Closure_declarations() {
    /** @type {?} */
    PipesModule.decorators;
    /**
     * @nocollapse
     * @type {?}
     */
    PipesModule.ctorParameters;
}
//# sourceMappingURL=pipes.module.js.map