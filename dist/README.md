# BlackboxComponentLibrary

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.3.

This app is a collection of modules that can be individually used on applications maintained with CRMBlackbox. Use best practices from the Angular CLI to generate modules, such as using the cli generate commands to create modules, directives, pipes, services, ect. Modules should have acceptable test coverage, documentation on how to use the modules, and can pass the linter.

## Generating a new module for a entity
1. `ng generate module modules/foo`
2. `ng generate component modules/foo/foo`
3. Add in the code for your component
4. Modify the module so that it exports the component
5. Along with unit tests, verify the component works with the AppComponent.  This is a sandbox enviornment for development, so nothing should be pushed up in the AppComponent.
6. Import the module into the root level module
7. Add `export * from './src/app/modules/foo/foo.module';` to `public_api.ts`
8. Update version propert on package.json (I figure we can start this with patch updates unless you modify some existing code, where that would be a minor update)
8. Run `npm run packagr`
9. `cd dist`
10. `npm pack`
11. commit your changes, push up to remote

To use this library from another project, clone a local version, then run:
`npm install ../blackboxComponentLibrary/dist/blackbox-component-library-0.0.0.tgz`

Then add `import { FooModule } from 'blackbox-component-library';`

All components are prefixed with `blackbox`, so to use the foo component in a template, write `<blackbox-foo></blackbox-foo>`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
