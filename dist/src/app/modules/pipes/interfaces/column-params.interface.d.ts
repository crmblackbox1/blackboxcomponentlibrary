export interface ColumnParams {
    label: string;
    mapper: (entity: any) => string;
    comparator?: (entity1: any, entity2: any) => number;
}
