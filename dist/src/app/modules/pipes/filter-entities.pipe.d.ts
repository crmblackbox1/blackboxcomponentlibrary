import { PipeTransform } from '@angular/core';
import { ColumnParams } from './interfaces/column-params.interface';
export declare class FilterEntitiesPipe implements PipeTransform {
    containsValue(entity: any, filter: string, columnParams: ColumnParams[]): boolean;
    isSubstring(value: string, filter: string): boolean;
    transform(entities: any[], filter: string, columnParams: ColumnParams[]): any[];
}
