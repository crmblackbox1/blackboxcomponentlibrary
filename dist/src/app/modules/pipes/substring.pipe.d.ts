import { PipeTransform } from '@angular/core';
/**
 * Return the last path of the url
 * @param value url
 * @param startCharacter: a character that will be used to split on
 * 	if undefined, default with '/' character
 *
 * Usage:
 *   value | substring
 *
 *  Example:
 *   {{'https://s3.amazonaws.com/placeholderlogo.png' | substring:'/' }}
 *   formats to: 'placeholderlogo.png'
 *
 * Example with different startCharacter:
 *   {{ 'abcd' | substring: 'b' }} formats to 'cd'
 */
export declare class SubstringPipe implements PipeTransform {
    transform(value: string, startCharacter?: string): string;
}
