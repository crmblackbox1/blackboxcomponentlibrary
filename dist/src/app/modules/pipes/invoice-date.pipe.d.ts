import { PipeTransform } from '@angular/core';
export declare class InvoiceDatePipe implements PipeTransform {
    /**
     * @param value string that looks like 2019-10-01, which is what Braintree gives
     * @param last boolen, if true, send back string with year
     *
     * Usage:
     *   value | invoiceDate
     *
     *  Example:
     *   {{'2019-09-12' | invoiceDate}}
     *   formats to: 'September 12'
     *
     * Example with last as true:
     *   {{ 2020-01-03' | invoiceDate:true }} formats to 'January 03, 2020'
     */
    transform(value: string, last?: boolean): string;
    hasValidValue(value: string, last: boolean): boolean;
    longMonth(month: string): string;
}
